//NOM et Prénom : Maxime Girard 
//NOM et Prénom : Maxime Girard 
//Code permanent : GIRM30058500

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "fonctions.h"

int * get_vecteur(FILE *file, int nombre_ligne)
{ 
  int *vecteur = malloc(nombre_ligne*sizeof(int)), char_courrant, max, i, j, k, u;
  char une_ligne[64999];
  i = j = k = u = max = 0;
  rewind(file);
  for(;i < nombre_ligne; i++,j++)
  {
    while((char_courrant = fgetc(file)) != '\n')
	  une_ligne[k++] = char_courrant;
	char * curr = strtok(une_ligne," ");	
	while(curr != NULL)
	{
	  u++;
	  curr = strtok(NULL, " ");
	}
    *(vecteur+j) = u;
	k = u = 0;
	memset(une_ligne, 0 , sizeof(une_ligne));
  }
  return vecteur;
}

/*
 * Fonction principale
 */
 
int main(int argc, char * argv[])
{
    int nb_ligne,nb_colonne, *vecteur,*caduque,i;
	
	//Validation présence fichier sur ligne de commande
	if(!(argc >= 2)) 
	{
		signaler_erreur(OUVERTURE_FICHIER_ERREUR);
		return EXIT_FAILURE;
	}
	
	//Ouverture du fichier
	FILE *fichier = fopen(argv[1], "r");
	
	//Vérification de présence fichier sur disque
    if(fichier == NULL) { signaler_erreur(OUVERTURE_FICHIER_ERREUR); return EXIT_FAILURE; }
	
	//Nombre de LIGNES
	nb_ligne = nbre_lignes_fichier(fichier);
	
	//Vérifier fichier vide
	if(nb_ligne == 0){ signaler_erreur(FICHIER_SANS_ENTIER_ERREUR); return EXIT_FAILURE; }

	//Déclaration & Initialisation vecteur 
    vecteur = malloc(nb_ligne*sizeof(int));
    caduque = malloc(nb_ligne*sizeof(int));
	caduque = get_vecteur(fichier, nb_ligne);
	memcpy(vecteur , caduque , nb_ligne*sizeof(int));
    free(caduque);	
	
	//Nombre de COLONNES
	nb_colonne = taille_max_lignes(vecteur,nb_ligne);
	
	//Verification du nombre de LIGNES NON VIDE
	int vrai_ligne = 1;
    for(i = 0; i < nb_ligne; i++) 
    {
     if(vecteur[i] != 0) vrai_ligne++; 
    }
    
 //Chargement de la MATRICE
 int * matrice = charger(fichier, vecteur, nb_ligne, nb_colonne);
 int *tableau_colonne = malloc(nb_colonne*sizeof(int));
 int *tableau_ligne = malloc((vrai_ligne)*sizeof(int));
 
 //Initialisation des tableaux indicateurs
 tableau_colonne = control(argv,nb_colonne,'C');
 tableau_ligne = control(argv,vrai_ligne, 'L');

 if(tableau_ligne == NULL && tableau_colonne == NULL)
 {
   affiche_Tab2D(matrice, vrai_ligne, nb_colonne);
   return EXIT_SUCCESS;
 }
 
 //Chargement de la nouvelle matrice 
 int * matrice_reduite = filter(matrice, &vrai_ligne, &nb_colonne, tableau_ligne, tableau_colonne);

 //Affichage final
 affiche_Tab2D(matrice_reduite, vrai_ligne, nb_colonne);
 
 //Liberer la mémoire
 free(matrice_reduite);
 free(matrice);
 free(tableau_colonne);
 free(tableau_ligne);
 free(vecteur);
 
 //Fin du programme
 return EXIT_SUCCESS;
}
