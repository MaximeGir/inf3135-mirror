//NOM et Pr�nom : Maxime Girard 
//Code permanent : GIRM30058500

#include <stdio.h>
#include "fonctions.h"
#include <stdlib.h>

//D�claration explicite d'une fonction utile de ce programme.
int
isdigit(int c);

int
isspace(int c);

//Affiche un message d'erreur sur le canal d'erreur selon les cas d�finis
void
signaler_erreur(int err)
{
    switch (err)
    {
        case OUVERTURE_FICHIER_ERREUR :
            fprintf(stderr,"Erreur d'ouverture du fichier.\n");
        break;
        case SYNTAX_DOMAIN_ERREUR :
            fprintf(stderr,"Erreur de syntaxe dans un domaine.\n");
            break;
        case OPTION_INCONNUE_ERREUR :
            fprintf(stderr,"Option inconnue.\n");
            fprintf(stderr,"Usage : filtre <fichier> [-C <liste>] [-L <liste>]\n");
            fprintf(stderr,"         filtre <fichier> [-L <liste>] [-C <liste>]\n");
            break;
        case OPTION_DUPLIQUEE_ERREUR :
                fprintf(stderr,"Option en double.\n");
                break;
        case FICHIER_SANS_ENTIER_ERREUR : 
            fprintf(stderr,"Le fichier ne comporte aucun entier.\n");
            break;
        case TABLEAU2D_VIDE_ERREUR :
            fprintf(stderr,"Tableau vide!\n");
            break;
        default : 
            fprintf(stderr,"Erreur inconnue.\n");
            break;
    }
    
}


/*
 * Affiche un tableau 2D nxm
 */
 
void
affiche_Tab2D(int *ptr, int n, int m)
{
    if (n>0 && m>0 && ptr!=NULL)
    {
        int (*lignePtr)[m]; 

        lignePtr = (int (*)[m]) ptr; 

        for (int i = 0 ; i < n ; i++)
        {
            for (int j = 0 ; j < m ; j++) 
            {
                printf("%5d ",lignePtr[i][j]);
            }
            printf("\b\n");
        }    
    }
    else
    {
        signaler_erreur(TABLEAU2D_VIDE_ERREUR);
    }
}

/*
 * Vecteur[i] repr�sente le nombre d'entiers � ligne i du fichier point� par fp.
 * si vecteur[i]==0, la ligne i du fichier est ignor�e car elle est vide d'entier.
 * Cette fonction retourne un pointeur sur la premi�re case d'un tableau 2D n x m
 * Le tableau 2D de taille n x max_vecteur est rempli � partir du fichier.
 * Chaque ligne du fichier qui contient au moins un entier donne lieu � une ligne dans le tableau 2D.
 */
 
int *
charger(FILE *fp, int * vecteur, int taille_vecteur, int max_vecteur)
{

 int ligne_non_nulle = 0,i;
 
 for(i = 0; i < taille_vecteur; i++) 
 {
  if(vecteur[i] == 0) continue;
  else ligne_non_nulle++;
 }
  
 rewind(fp);
 
 int (*tableau_2d)[max_vecteur] = malloc((ligne_non_nulle+1)*sizeof(*tableau_2d));
 int ligne_entier[max_vecteur], reset = 0, u = 0, j = 0, k = 0, v = 0, w = 0;
 int s = 3*max_vecteur;
 char ligne[s];
 
 while(fgets(ligne,s,fp))
 {
  char word[s];
  j = k = 0;
  for(i = 0; i < s; i++) word[i] = '\0';
  
  while(ligne[k])
  {
   switch(ligne[k])
   {  
	case ' ' : if(word[0] == '\0' ) break; 
	           else 
	 		   {
				ligne_entier[u++] = atoi(word);
				for(reset = 0; reset < s; reset++) word[reset] = '\0';
				j = 0;
				break;
			   }
	case '\n': if(word[0] == '\0') break;
	           else 
			   { 
			    ligne_entier[u++] = atoi(word);
                if(max_vecteur - u == 0) 
			    {
				 u = 0;	
				 for(w = 0; w < max_vecteur; w++,u++) 
				  { 
				   tableau_2d[v][w] = ligne_entier[u];
 				  }
			    }  
			    else if(max_vecteur - u > 0)
				{
				 for( u+=1 ; u < max_vecteur; u++) ligne_entier[u] = 0;
				 for(u = 0, w = 0; w < max_vecteur; w++,u++)
				 {
  				  tableau_2d[v][w] = ligne_entier[u]; 
				 }
				}
				for(reset = 0; reset < s; reset++) word[reset] = '\0';
				for(w = 0; w < max_vecteur; w++)
				{ 
				ligne_entier[w] = 0;
				}
				j = u = 0;
				v++;
				}
	            break;

	  default: word[j++] = ligne[k];
	           if(isdigit(ligne[k]) && !ligne[k+1])
			   { 
			    ligne_entier[u++] = atoi(word);
                if(max_vecteur - u == 0) 
			    {
				 u = 0;	
				 for(w = 0; w < max_vecteur; w++,u++) 
				  { 
				   tableau_2d[v][w] = ligne_entier[u];
 				  }
			    }  
			    else if(max_vecteur - u > 0)
				{
				 for( u+=1 ; u < max_vecteur; u++) ligne_entier[u] = 0;
				 for(u = 0, w = 0; w < max_vecteur; w++,u++)
				 {
  				  tableau_2d[v][w] = ligne_entier[u]; 
				 }
				}
				for(reset = 0; reset < s; reset++) word[reset] = '\0';
				for(w = 0; w < max_vecteur; w++)
				{ 
				ligne_entier[w] = 0;
				}
				j = u = 0;
				v++;
				}
				break;
	 }
	 k++;
    }
   }
      
   //Conversion en pointeur d'entier 
   int * retour = (int *) *tableau_2d;
   return retour;
}

/*
 * Retourne le max du tableau point� par vecteur et de taille v
 * retourne 0 si le tableau est vide.
 */

int 
taille_max_lignes(int * vecteur , int v)
{
 if(vecteur == NULL) return 0;
 int max = (*vecteur);
 for(int i = 0; i <= v; i++) 
 {
   if(max < *(vecteur+i))
   {  
     max = *(vecteur+i);
   }
 }
 return max;
}

/*
 * Retourne le nombre de lignes total du fichier point� par fp  
 */
int 
nbre_lignes_fichier(FILE *fp)
{
 int car , nb_ligne = 0;
 rewind(fp);
  while(!feof(fp))
  {
   if((car = fgetc(fp)) == '\n') nb_ligne++;   
  }
 return nb_ligne;
}
 
/*
 * La variable argv repr�sente le tableau pointant les param�res du programme
 * retourne l'index de option dans argv. par exemple si option est 'C'
 * la valeur retourn�e est l'indexe de la chaine "-C" dans argv
 */ 
 
int 
seek_option(char *const argv[], char option)
{
  int index = 0, index_relatif = 0, occurence = 0;
  char intru;
  while(argv[index])
  {
    if(*argv[index] == '-')
	{
	 if(*(argv[index]+1) == option)
	 {
	   ++occurence;
	   index_relatif = index;
	 }
	 
	 switch(*(argv[index]+1))
	 {
	 case 'L': break;
	 case 'C': break;
	 default: 
	   if(*(argv[index]+1) != intru && (*(argv[index]+1) <= 9 && *(argv[index]+1)))
	   {
        intru = *(argv[index]+1);
	    signaler_erreur(OPTION_INCONNUE_ERREUR);		
		break;
	   }
	 }
	}
	if(argv[index+1] == NULL) break;
	index++;
  }
  
  if(occurence == 1) 
  { 
   return index_relatif;
  }
  else if(occurence > 1) signaler_erreur(OPTION_DUPLIQUEE_ERREUR);
  else if(occurence == 0) return 0;
  return -1;
}

/*
 *argv repr�sente le tableau des param�res du programme
 *cette fonction retourne la taille de la liste de domaine � partir argv[pos] 
 *la fin de la liste est localis�e soit par la fin du tableau d'argument soit par une option connue
 */
 
int 
get_nbre_domaines(char *const argv[], int pos)
{
 int i = pos, count = 0;
 while(argv[i])
 {
  char * argument = argv[i++];
  if(argument[0] == '-') 
  {
   if(argument[1] == 'C' || argument[1] == 'L') 
   {
    while((argument = argv[i++]))
	{
	 if(argument[0] == '-') 
     {
      if(argument[1] == 'C' || argument[1] == 'L')
	  {
	   break;
	  }
      else if(argument[1]-'0') { count++; break; }	  
	 }
	 else count++;
	}
   }
   break;
  }
 }
 return count;
}
int check_domaine(char *domaine)
{
  if(domaine != NULL) return 1;
  else return 0;
}

/* 
 * Retourne 1 si la syntaxe  de domaine est correcte et place le d�but et la fin du domaine dans debut et fin
 * retourne 0 si la syntaxe  de domaine est incorrecte
 * max est utilis� pour que <nun>- represente le domaine <num>-max
 */
 
int 
get_debut_fin_domaine(char *domaine, int max, int *debut, int *fin)
{
 if(check_domaine(domaine))
 {
  int i = 0;
  while(domaine[i])
  {
   if(domaine[i] == '-' && isdigit(domaine[i+1]))
   {
     *debut = 0;
	 *fin = domaine[i+1]-'0';
     return 1;	
   } 
   if(isdigit(domaine[i]) && domaine[i+1] == '-' && !domaine[i+2])
   {
     *debut = domaine[i]-'0';
 	 *fin = max;
	 return 1;
   }
   if(isdigit(domaine[i]) && domaine[i+1] == '-' && isdigit(domaine[i+2]))
   {
    *debut = domaine[i]-'0';
    *fin = domaine[i+2]-'0';
    return 1;
   }
   if(isdigit(domaine[i]) && !domaine[i+1]) 
   {
    *debut = domaine[i]-'0';
    *fin = 0;
    return 1;
   }
   return 0;
  }
 }
 return 0;
}


/*
 * La variable c est soit 'C', soit 'L'.
 * dim est cens�e repr�senter soit le nombre de lignes dans le tableau 2D soit le nombre de colonnnes.
 * argv repr�sente le tableau pointant les param�res du programme.
 * retourne un tableau d'entiers de taille dim rempli avec des 0 et des 1 :
 * une case est remplie avec 0 si elle correspond � une ligne ou une colonne qui ne va pas 
 * �tre supprim�e selon argv et l'option c ;
 * une case est remplie avec 1 si elle correspond � une ligne ou une colonne qui va 
 * �tre supprim�e selon argv et l'option c
 */

int * 
control(char *const argv[], int dim, char c)
{ 
  if(!argv[2]) return NULL;
  if(c == 'C')
  {
   int index_colonne = seek_option(argv, 'C');
   int domaine_colonne = 0,debut = 0, fin = 0;
   
   if(index_colonne == -1 || index_colonne == 0){ return NULL; }
   if(index_colonne >= 1)
   {
    domaine_colonne = get_nbre_domaines(argv,index_colonne); 	
	char **domaine_cx = malloc(dim*sizeof(char *)) ; 
    int k = index_colonne, h = 0, i = 0;
    while(k++ < domaine_colonne+index_colonne+1)
    {
     domaine_cx[h] = argv[k];
 	 h++;
    }
	
	//Initialisation du tableau + affectation
	int *tableau_colonne = malloc(dim*sizeof(int)), u = 0;
    for(u = 0; u < dim; u++) tableau_colonne[u] = 0;
    i = 0;
	
	//Boucle sur arguments et affectation du tableau de colonne 
	while(domaine_cx[i]) 
    {
      debut = fin = 0;
	  get_debut_fin_domaine(domaine_cx[i], dim, &debut, &fin);

	  if((fin == 0) || (debut == fin)){ tableau_colonne[debut] = 1; }
	  while(debut <= fin) tableau_colonne[debut++] = 1;
	  i++;
	}
    free(domaine_cx);
	return tableau_colonne;
   }
  }
  
  if(c == 'L')
  {
   int index_ligne = seek_option(argv, 'L');
   int domaine_ligne = 0, debut = 0, fin = 0;
   
   if(index_ligne == -1 || index_ligne == 0){ return NULL; }
   if(index_ligne >= 1)
   {
    domaine_ligne = get_nbre_domaines(argv,index_ligne);
    char **domaine_lx = malloc(domaine_ligne*sizeof(char*)); 
    int k = index_ligne, h = 0, i = 0;
    while(k++ < domaine_ligne+index_ligne+1)
    {
     domaine_lx[h] = malloc(sizeof(int));
     domaine_lx[h] = argv[k];
     h++;
    }
	
    int *tableau_ligne = malloc(dim*sizeof(int)), v = 0;
    for(v = 0; v < dim; v++) tableau_ligne[v] = 0;
	i = 0;
	while(domaine_lx[i]) 
    {
     debut = fin = 0;
     if(get_debut_fin_domaine(domaine_lx[i], dim, &debut, &fin))
	 {

	  if((fin == 0) || (debut == fin)) tableau_ligne[debut] = 1;
	  while(debut <= fin) tableau_ligne[debut++] = 1;
	 }
	 i++;
    }
     free(domaine_lx);
	 return tableau_ligne;
   }
  }
  return NULL;
}


/*
 * La variable mat pointe le d�but d'un tableau 2D de taill *n x *m.
 * controlC de taille *m est un vecteur indiquant les colonnes � supprimer selon la fonction control.
 * controlV de taille *n est un vecteur indiquant les lignes � supprimer selon la fonction control.
 * retourne un pointeur sur le d�but d'un tableau 2D de la bonne taille apr�s application 
 * des suppressions donn�es par controlC et controlL  et place la nouvelle taille du tableau dans *n et *m.
 * retourne NULL sil le tableau r�sultant est vide.
 */

int *
filter(int * mat, int *n, int *m, int *controlL, int *controlC)
{
 int nouveau_n = *n, nouveau_m = *m;
 int i, j, x, y, z, k, count_ligne = 0, count_colonne = 0;

 if(controlL != NULL)
 {
  for(i = 0; i < *n; i++) { if(controlL[i] == 1){ count_ligne++; }}
  nouveau_n = *n - count_ligne; 
 }

 if(controlC != NULL)
 {
  for(j = 0; j < *m; j++) { if(controlC[j] == 1){ count_colonne++; }} 
  nouveau_m = *m - count_colonne; 
 }
   
  //Conversion du pointeur d'entier vers un pointeur de tableau d'entier.
  
  int (*matrice)[(*m)];  
  matrice = (int (*)[(*m)]) mat;

  //D�claration & Init nouvelle matrice selon nouvelles dimensions.
  int (*matrice_sans_c)[nouveau_m] = malloc(nouveau_n*sizeof(*matrice_sans_c));
  int *tableau = malloc((*m)*sizeof(int));
  int valeur = 0, compte = 0;
  for(k = 0; k < *n; k++)
  {
   if(controlL == NULL)
   {
    for(j = 0, x = 0; x < *m; x++, j++)
    {
      tableau[j] = matrice[k][x];
    }

	//Si aucune colonne � enlever
	if(nouveau_m == *m)
	{ 
	 for(z = 0; z < *m; z++)
	 { 
	  matrice_sans_c[k][z] = tableau[z]; 
	 }  
	}
	
	//Si colonne � enlever
	else 
	{
  	 for(z = 0, y = 0; y < *m; y++)
	 {
	  if(controlC[y] == 0)
	  {
	   matrice_sans_c[k][z++] = tableau[y];
	  }
	 }
    }
   }
   if(controlL != NULL)
   {
    if(controlL[k] == 0)
    {
     for(j = 0, x = 0; x < *m; x++, j++)
     {
	  tableau[j] = matrice[k][x];
     }
	} else { compte++; valeur = k - compte + 1; continue; }
	
	//Si aucune colonne � enlever
	if(controlC == NULL)
	{ 
	 if(compte == 0)
	 {
	  for(z = 0; z < *m; z++)
	  { 
	   matrice_sans_c[k][z] = tableau[z];	  
	  }
     }
     if(compte != 0)
	 {
  	  for(z = 0; z < *m; z++)
	  { 
	   matrice_sans_c[valeur][z] = tableau[z];
	  }
	 }	 
	}
	
	//Si colonne � enlever
	else 
	{
  	 for(z = 0, y = 0; y < *m; y++)
	 {
	  if(controlC[y] == 0 && compte == 0)
	  {
	   matrice_sans_c[k][z++] = tableau[y];
	  }
	  if(controlC[y] == 0 && compte != 0)
	  {
	   matrice_sans_c[valeur][z++] = tableau[y];
	  }
	 }
    }
   }
  } 
  
 *m = nouveau_m;
 *n = nouveau_n; 
 
 return (int *) *matrice_sans_c;
}
