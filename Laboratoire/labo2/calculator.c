#include <stdio.h>
#include <stdlib.h>

int calcule (int op1, int op2, char operateur){
 if (operateur == '*') return op1 * op2;
 else if (operateur == '/'){
  if (op2 != 0) {
    return op1 / op2;
  }
  else 
  {
   printf("Erreur : Division par zero!");
   return 0;
  }
 }
 else if (operateur == '+') return op1 + op2;
 else if (operateur == '-') return op1 - op2;
 else 
 {
 printf("Erreur: Operateur non valide!!");
 return 0;
 }
}

int main(){
 
 printf("resultat : %d \n", calcule(5, 10, '*'));
 printf("resultat : %d \n", calcule(5, 10, '-'));
 printf("resultat : %d \n", calcule(50, 10, '/'));
 printf("resultat : %d \n", calcule(5, 10, '+'));
 printf("resultat : %d \n", calcule(5, 10, 'v'));
 printf("resultat : %d \n", calcule(5, 10, '@'));

}
