#include <stdio.h>
#include <stdlib.h>

int main(){
 
int n;
int i;
int total = 0;

printf("Entrez un nombre de case: ");
scanf("%d", &n);

int tab[n];

for(i = 0; i <=n; i++){
 tab[i] = i*i;
 printf("tab[%d] = %d \n" , i,tab[i] );
 total += tab[i];
}

printf("Le total du tableau de %d element = %d \n ", n, total);


return EXIT_SUCCESS;
}
