#include <stdlib.h>
#include <stdio.h>

int inverse(int tab[],int size){
	int i;
	int x;
	int tab2[size];

	for(i = size-1, x = 0 ; i >= 0; i--, x++){
		tab2[x] = tab[i];  
	}
	for(i = 0; i < size; i++){
		tab[i] = tab2[i];
	} 

	for(i = 0; i < size; i++){
		printf("tab[%d] = %d .\n", i, tab[i]);
	}
	return 0;
}

int main(){
	int k;
	int j;
	int e;
	printf("Entrez le nombre de case: ");
	scanf("%d", &k);
	int tube[k]; 
	for(j = 0; j < k; j++){
		printf("Entrez la valeur de l'indice %d : ", j);
		scanf("%d", &e);
		tube[j] = e;
	}
	int size = sizeof(tube)/sizeof(int);
	printf("sizeof array : %d\n", size); 
	inverse(tube, size);
	return EXIT_SUCCESS;
}


