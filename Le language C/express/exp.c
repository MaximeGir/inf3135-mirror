#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double pow(double x, double y);

int main()
{

int b = 1;
int i = 0;

while(b){
 printf("\n b = %d", b);
 if (b++ < 20) { continue; }
 else break;
 }
printf("\n b apres la boucle = %d\n", b);
printf("\n b power b = %2.f\n", pow(b,b));
for(; i < 20; i++)
{
 if(i%2 == 0) continue;
 if(i%2 == 1) printf("%d\n",i); 
}

return EXIT_SUCCESS;
}
