#include <stdlib.h>
#include <stdio.h>

#define MAX_ELEMENT_PILE 100

struct Element
{
  union 
  {
   int entier;
   char * chaine;
  } value;
  struct Element * prochain;
} ;

struct Pile
{
  struct Element contenue[MAX_ELEMENT_PILE];
  int taille ;
} pile ;

static int i = 0;

//@params choix = i si pile d'entier
//@params choix = c si pile de string

struct Pile create_pile(char choix)
{
 if(choix == 'i')
  pile.contenue[0].value.entier = 0 ;
 if(choix == 'c')
  pile.contenue[0].value.chaine = "0";
 return pile;
}

int estVide(struct Pile pile)
{
 if(pile.taille == 0) return 1;
 return 0;
}

int estPleine(struct Pile pile)
{
 if(pile.taille == MAX_ELEMENT_PILE) return 1;
 return 0;
}

int empiler(struct Element element_a_empiler)
{
 if(estVide(pile)) 
 {
  pile.contenue[0] = element_a_empiler;
  return 1;
 }
 else if(estPleine(pile)) 
 {
  pile.contenue[i++] = element_a_empiler;
  return 1;
 }
 return 0;
}

struct Element depiler(struct Pile pile)
{
 return pile.contenue[i--];
}

