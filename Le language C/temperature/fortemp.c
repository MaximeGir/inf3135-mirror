#include <stdio.h>
#include <stdlib.h>

#define MAX 300 
#define PAS 20 
#define MIN 0
#define QUOTIENT (5.0/9.0)
#define PAS_ARRIERE 32

int main(){
  int fahr;
  for(fahr = MAX; fahr >= MIN; fahr -= PAS){
    printf("%3d %6.1f\n", fahr, QUOTIENT * (fahr - PAS_ARRIERE));
  }
  return EXIT_SUCCESS; 
}
