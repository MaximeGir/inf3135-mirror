#include <stdlib.h>
#include <stdio.h>

int reset(int * val_to_reset)
{
  int old = *val_to_reset;
  *val_to_reset = 0;
  return old;
}

int main()
{
 int val = 18;
 int old = reset(&val); 
 printf("%d,%d\n", val, old);
 return EXIT_SUCCESS;
}
