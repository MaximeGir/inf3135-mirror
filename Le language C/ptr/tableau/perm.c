#include <stdlib.h>
#include <stdio.h>

void permuter(int * a, int * b)
{
  int temp = *a;
  *a = *b;
  *b = temp;
}

int main()
{
 int x = 10, b = 20;
 int * a = &x;
 permuter(a,&b);
 printf("a = %d, b = %d \n", *a,b); 
 return EXIT_SUCCESS;
}
