#include <stdio.h>

int main()
{
  int k = 10;
  int * ptr = &k;
  printf("%p\n", ptr); // va imprimer addresse
  printf("%d\n", *ptr); //va imprimer la valeur situé à l'adresse
  
//  printf("%p\n", ++ptr); // va afficher adresse de k + 4 octet  : dont &k + sizeof(int)
//  printf("%d\n", *ptr); // va afficher la valeur de l'adresse &k + sizeof(int)

  int * incP = &*ptr;
  printf("Cest incP = &*ptr : %p\n et ptr : %p et *incp : %d\n", incP, ptr, *incP);
  printf("%p cest K\n\v", &k); 
  printf("cest ++*incP , comme ++k en fait : %d\n ", ++*incP);
  printf("Valeur de *ptr : %d\n ", *ptr);

  printf("%d ", ++*incP);
  printf("%d ", *ptr);
  return 0;
}
