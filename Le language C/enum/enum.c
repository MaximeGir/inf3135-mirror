#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

bool implies(bool p , bool q)
{
  if (p == true && q == false) return false;
  else return true;
}

int main()
{
 enum jour 
 { samedi, dimanche };

 enum jour fin_semaine = samedi;
 fin_semaine += dimanche; 
 
 printf("%d\n",fin_semaine);

 bool p = true;
 bool q = false;
 
 if(implies(p,q)) printf("Implication vrai!\n");
 else if (!implies(p,q)) printf("Implication fausse!\n");

  
 return EXIT_SUCCESS;
}
