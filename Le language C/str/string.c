#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int stringlen(char string[]){
 int i = 0;
 while(string[i] != '\0') i++;
 return i; 
}

int main()
{
 char * chaine = "12345678910";
 int j = stringlen(chaine);
 printf("Longeur de la chaine = %d .\n",j);  
 return EXIT_SUCCESS;
}
